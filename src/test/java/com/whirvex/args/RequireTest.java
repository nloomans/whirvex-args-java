package com.whirvex.args;

import static org.junit.Assert.*;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Predicate;

import org.junit.Before;
import org.junit.Test;

/**
 * The unit tests for {@link Require} and related components.
 */
public class RequireTest {

	private Option opt;
	private Require require;

	@Before
	public void setup() {
		Map<Option, List<String>> opts = new HashMap<>();
		this.opt = new Option("opt", null, false, "-o");
		opts.put(opt, Arrays.asList(""));

		Args args = new Args(Collections.emptyList(), opts);
		this.require = args.require;
	}

	@Test
	public void cond() {
		require.cond((args) -> true);
		Predicate<Args> fail = ((args) -> false);
		assertThrows(ArgsException.class, () -> require.cond(fail));
		assertThrows(ArgsException.class, () -> require.cond(fail, "msg"));
	}

	@Test
	public void argc() {
		require.argc(0);
		assertThrows(ArgsException.class, () -> require.argc(1));
	}

	@Test
	public void optc() {
		require.optc(1);
		assertThrows(ArgsException.class, () -> require.optc(2));

		require.optc(opt, 1);
		assertThrows(ArgsException.class, () -> require.optc(opt, 2));
		require.namec(opt.name, 1);
		assertThrows(ArgsException.class, () -> require.namec(opt.name, 2));
		require.flagc(opt.flags[0], 1);
		assertThrows(ArgsException.class, () -> require.flagc(opt.flags[0], 2));
	}

	@Test
	public void has() {
		Option key = new Option("key", null, false, "-k");

		require.has(opt);
		assertThrows(ArgsException.class, () -> require.has(key));
		require.hasName(opt.name);
		assertThrows(ArgsException.class, () -> require.hasName(key.name));
		require.hasFlag(opt.flags[0]);
		assertThrows(ArgsException.class, () -> require.hasFlag(key.flags[0]));
	}

}
