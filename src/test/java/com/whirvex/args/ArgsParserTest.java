package com.whirvex.args;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

/**
 * The unit tests for the standard {@link ArgsParser} and related components.
 */
public class ArgsParserTest {

	private Option opt;
	private Option key;

	@Before
	public void setup() {
		this.opt = new Option("opt", null, true, "-o", "--opt");
		this.key = new Option("opt", null, false, "-k", "--key");
	}

	@Test
	public void parse() {
		assertThrows(NullPointerException.class,
				() -> Args.parse(new String[0], (Option[]) null));
		assertThrows(NullPointerException.class,
				() -> Args.parse((String[]) null, new Option[0]));
	}

	@Test
	public void validate() {
		String[] args = new String[0];

		/*
		 * The standard arguments parser should not accept options which share
		 * identical names with one another.
		 */
		Option[] names = new Option[2];
		for (int i = 0; i < names.length; i++) {
			names[i] = new Option("duplicate", null, false);
		}
		assertThrows(DuplicateNameException.class,
				() -> Args.parse(args, names));

		/*
		 * The standard arguments parser should not accept options which share
		 * identical flags with one another.
		 */
		Option[] flags = new Option[2];
		for (int i = 0; i < flags.length; i++) {
			flags[i] = new Option("" + i, null, false, "-d");
		}
		assertThrows(DuplicateFlagException.class,
				() -> Args.parse(args, flags));

		/*
		 * The standard arguments parser should not accept option flags that: do
		 * not begin with "-" or "--", contain whitespace, are a short flag not
		 * of length 2, or are a long flag without an identifier.
		 */
		Option[] key = new Option[] { new Option("opt", null, false, "") };
		assertThrows(IllegalFlagException.class, () -> Args.parse(args, key));
		key[0].flags[0] = "--key ";
		assertThrows(IllegalFlagException.class, () -> Args.parse(args, key));
		key[0].flags[0] = "-key";
		assertThrows(IllegalFlagException.class, () -> Args.parse(args, key));
		key[0].flags[0] = "--";
		assertThrows(IllegalFlagException.class, () -> Args.parse(args, key));
	}

	@Test
	public void indexed() {
		String[] input = { "0", "1", "2" };
		Args args = Args.parse(input);

		assertEquals(input.length, args.argc());
		for (int i = 0; i < args.argc(); i++) {
			assertEquals(args.get(i), "" + i);
		}
	}

	@Test
	public void opts() {
		String[] input = { "-o", "val", "--opt", "value" };
		Args args = Args.parse(input, opt);

		assertEquals(2, args.optc(opt));
		assertEquals("val", args.get(opt, 0));
		assertEquals("value", args.get(opt, 1));
	}

	/*
	 * The standard arguments parser should be able to handle an argument coming
	 * after an option that takes in no value.
	 */
	@Test
	public void opts_argumentNext() {
		String[] input = { "-k", "value", "--key", "value" };
		Args args = Args.parse(input, key);

		assertEquals(2, args.argc());
		assertEquals("value", args.get(0));
		assertEquals("value", args.get(1));
		assertEquals(2, args.optc(key));
	}

	/*
	 * The standard arguments parser should be able to handle option keys and
	 * values that have somehow been cacooned into the same string in an input
	 * array. This usually happens when arguments are specified via Java program
	 * arguments.
	 */
	@Test
	public void opts_cacoonValue() {
		String[] input = { "-o val0", "-o=val1", "-o = val2", "--opt value0",
				"--opt=value1", "--opt = value2" };
		Args args = Args.parse(input, opt);

		assertEquals(6, args.optc(opt));
		assertEquals("val0", args.get(opt, 0));
		assertEquals("val1", args.get(opt, 1));
		assertEquals("val2", args.get(opt, 2));
		assertEquals("value0", args.get(opt, 3));
		assertEquals("value1", args.get(opt, 4));
		assertEquals("value2", args.get(opt, 5));
	}

	/*
	 * The standard arguments parser should be able to handle a cacooned
	 * argument that has been cacooned with an option that takes in no value.
	 */
	@Test
	public void opts_cacoonArgument() {
		String[] input = { "-k val", "--key value" };
		Args args = Args.parse(input, key);

		assertEquals(2, args.argc());
		assertEquals("val", args.get(0));
		assertEquals("value", args.get(1));
		assertEquals(2, args.optc(key));
	}

	/*
	 * The standard arguments parser should be able to accept multiple short
	 * keys specified in the same key string. This only works for short keys, as
	 * they are guaranteed to always have a length of exactly one character. If
	 * any of these options take in a value, it must also be the last one at the
	 * end of the key string.
	 */
	@Test
	public void opts_squishKeys() {
		String[] input = { "-" };

		Option[] opts = new Option[3];
		for (int i = 0; i < opts.length; i++) {
			opts[i] = new Option("" + i, null, false, "-" + i);
			input[0] += i;
		}

		Args args = Args.parse(input, opts);
		assertEquals(opts.length, args.optc());
		for (Option opt : opts) {
			assertTrue(args.has(opt));
		}
	}

	/*
	 * The standard arguments parser should be able to accept a value for an
	 * option specified by it's short key without the use of a space or an
	 * equals symbol. This only works for short keys, since they have a fixed
	 * length of one. For an option to take in a value this way, it must be the
	 * last option in the short option key string. It must also be the only
	 * option in said key string that takes in a value.
	 */
	@Test
	public void opts_squishValue() {
		String[] input = { "-oval", "-ov val", "-ov=val" };
		Args args = Args.parse(input, opt);

		assertEquals(3, args.optc(opt));
		assertEquals("val", args.get(opt, 0));
		assertEquals("v val", args.get(opt, 1));
		assertEquals("v=val", args.get(opt, 2));
	}

	/*
	 * The standard arguments parser should be able to have options parsing
	 * disabled. This allows an option key may be presented as an indexed
	 * argument. However, once options parsing has been disabled, it cannot be
	 * re-enabled.
	 */
	@Test
	public void opts_disable() {
		String[] input = { "--", "--opt" };
		Args args = Args.parse(input);

		assertEquals(1, args.argc());
		assertEquals("--opt", args.get(0));
	}

	/*
	 * The standard arguments parser should throw an exception if it encounters
	 * an option that it has not been told of.
	 */
	@Test
	public void opts_unknown() {
		String[][] inputs = { { "-o" }, { "--opt" } };
		for (String[] input : inputs) {
			assertThrows(UnknownOptionException.class, () -> Args.parse(input));
		}
	}

	/*
	 * The standard arguments parser should thrown an exception if it encounters
	 * an option that requires a value but has none specified.
	 */
	@Test
	public void opts_missing() {
		String[][] inputs =
				{ { "-o" }, { "--opt" }, { "-o", "--" }, { "--opt", "--" } };
		for (String[] input : inputs) {
			assertThrows(MissingValueException.class,
					() -> Args.parse(input, opt));
		}
	}

}
