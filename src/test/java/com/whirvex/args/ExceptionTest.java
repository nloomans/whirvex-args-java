package com.whirvex.args;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import org.junit.Test;

/**
 * The unit tests for {@link ArgsException} and related components.
 */
public class ExceptionTest {

	private final ArgsParser parser = new StdArgsParser();

	@Test
	public void construct_ArgsException() {
		int index = 0;
		Option opt = new Option("opt", null, false, "-o");
		String message = "message";
		Throwable cause = new Exception();

		ArgsException e = new ArgsException(-1, message, cause);
		assertEquals(message, e.getMessage());
		assertEquals(cause, e.getCause());
		assertEquals(-1, e.getIndex());
		assertNull(e.getOption());
		assertNull(e.getWhat());

		e = new ArgsException(index, message, cause);
		assertEquals(message, e.getMessage());
		assertEquals(cause, e.getCause());
		assertEquals(index, e.getIndex());
		assertNull(e.getOption());
		assertEquals("for argument 0", e.getWhat());

		e = new ArgsException(index, message);
		assertEquals(message, e.getMessage());
		assertNull(e.getCause());
		assertEquals(index, e.getIndex());
		assertNull(e.getOption());
		assertEquals("for argument 0", e.getWhat());

		e = new ArgsException(index, cause);
		assertNull(e.getMessage());
		assertEquals(cause, e.getCause());
		assertEquals(index, e.getIndex());
		assertNull(e.getOption());
		assertEquals("for argument 0", e.getWhat());

		e = new ArgsException(index);
		assertNull(e.getMessage());
		assertNull(e.getCause());
		assertEquals(index, e.getIndex());
		assertNull(e.getOption());
		assertEquals("for argument 0", e.getWhat());

		e = new ArgsException(opt, message, cause);
		assertEquals(message, e.getMessage());
		assertEquals(cause, e.getCause());
		assertEquals(-1, e.getIndex());
		assertEquals(opt, e.getOption());
		assertEquals("for option -o", e.getWhat());

		e = new ArgsException(opt, message);
		assertEquals(message, e.getMessage());
		assertNull(e.getCause());
		assertEquals(-1, e.getIndex());
		assertEquals(opt, e.getOption());
		assertEquals("for option -o", e.getWhat());

		e = new ArgsException(opt, cause);
		assertNull(e.getMessage());
		assertEquals(cause, e.getCause());
		assertEquals(-1, e.getIndex());
		assertEquals(opt, e.getOption());
		assertEquals("for option -o", e.getWhat());

		e = new ArgsException(opt);
		assertNull(e.getMessage());
		assertNull(e.getCause());
		assertEquals(-1, e.getIndex());
		assertEquals(opt, e.getOption());
		assertEquals("for option -o", e.getWhat());

		e = new ArgsException(message, cause);
		assertEquals(message, e.getMessage());
		assertEquals(cause, e.getCause());
		assertEquals(-1, e.getIndex());
		assertNull(e.getOption());
		assertNull(e.getWhat());

		e = new ArgsException(message);
		assertEquals(message, e.getMessage());
		assertNull(e.getCause());
		assertEquals(-1, e.getIndex());
		assertNull(e.getOption());
		assertNull(e.getWhat());

		e = new ArgsException(cause);
		assertNull(e.getMessage());
		assertEquals(cause, e.getCause());
		assertEquals(-1, e.getIndex());
		assertNull(e.getOption());
		assertNull(e.getWhat());

		e = new ArgsException();
		assertNull(e.getMessage());
		assertNull(e.getCause());
		assertEquals(-1, e.getIndex());
		assertNull(e.getOption());
		assertNull(e.getWhat());
	}

	@Test
	public void construct_ArgsParserException() {
		int index = 0;
		Option opt = new Option("opt", null, false, "-o");
		String message = "message";
		Throwable cause = new Exception();

		ArgsParserException e =
				new ArgsParserException(parser, index, message, cause);
		assertEquals(message, e.getMessage());
		assertEquals(cause, e.getCause());
		assertEquals(index, e.getIndex());
		assertNull(e.getOption());
		assertEquals(parser, e.getParser());

		e = new ArgsParserException(parser, index, message);
		assertEquals(message, e.getMessage());
		assertNull(e.getCause());
		assertEquals(index, e.getIndex());
		assertNull(e.getOption());
		assertEquals(parser, e.getParser());

		e = new ArgsParserException(parser, index, cause);
		assertNull(e.getMessage());
		assertEquals(cause, e.getCause());
		assertEquals(index, e.getIndex());
		assertNull(e.getOption());
		assertEquals(parser, e.getParser());

		e = new ArgsParserException(parser, index);
		assertNull(e.getMessage());
		assertNull(e.getCause());
		assertEquals(index, e.getIndex());
		assertNull(e.getOption());
		assertEquals(parser, e.getParser());

		e = new ArgsParserException(parser, opt, message, cause);
		assertEquals(message, e.getMessage());
		assertEquals(cause, e.getCause());
		assertEquals(-1, e.getIndex());
		assertEquals(opt, e.getOption());
		assertEquals(parser, e.getParser());

		e = new ArgsParserException(parser, opt, message);
		assertEquals(message, e.getMessage());
		assertNull(e.getCause());
		assertEquals(-1, e.getIndex());
		assertEquals(opt, e.getOption());
		assertEquals(parser, e.getParser());

		e = new ArgsParserException(parser, opt, cause);
		assertNull(e.getMessage());
		assertEquals(cause, e.getCause());
		assertEquals(-1, e.getIndex());
		assertEquals(opt, e.getOption());
		assertEquals(parser, e.getParser());

		e = new ArgsParserException(parser, opt);
		assertNull(e.getMessage());
		assertNull(e.getCause());
		assertEquals(-1, e.getIndex());
		assertEquals(opt, e.getOption());
		assertEquals(parser, e.getParser());

		e = new ArgsParserException(parser, message, cause);
		assertEquals(message, e.getMessage());
		assertEquals(cause, e.getCause());
		assertEquals(-1, e.getIndex());
		assertNull(e.getOption());
		assertEquals(parser, e.getParser());

		e = new ArgsParserException(parser, message);
		assertEquals(message, e.getMessage());
		assertNull(e.getCause());
		assertEquals(-1, e.getIndex());
		assertNull(e.getOption());
		assertEquals(parser, e.getParser());

		e = new ArgsParserException(parser, cause);
		assertNull(e.getMessage());
		assertEquals(cause, e.getCause());
		assertEquals(-1, e.getIndex());
		assertNull(e.getOption());
		assertEquals(parser, e.getParser());

		e = new ArgsParserException(parser);
		assertNull(e.getMessage());
		assertNull(e.getCause());
		assertEquals(-1, e.getIndex());
		assertNull(e.getOption());
		assertEquals(parser, e.getParser());

		e = new ArgsParserException();
		assertNull(e.getMessage());
		assertNull(e.getCause());
		assertEquals(-1, e.getIndex());
		assertNull(e.getOption());
		assertNull(e.getParser());
	}

	@Test
	public void construct_DuplicateNameException() {
		String name = "name";

		DuplicateNameException e = new DuplicateNameException(parser, name);
		assertEquals(name, e.getName());
		assertNull(e.getCause());
		assertEquals(-1, e.getIndex());
		assertNull(e.getOption());
		assertEquals(parser, e.getParser());
	}

	@Test
	public void construct_IllegalFlagException() {
		String message = "message";

		IllegalFlagException e = new IllegalFlagException(message);
		assertEquals(message, e.getMessage());
		assertNull(e.getCause());
	}

	@Test
	public void construct_DuplicateFlagException() {
		String flag = "--flag";

		DuplicateFlagException e = new DuplicateFlagException(parser, flag);
		assertEquals(flag, e.getFlag());
		assertNull(e.getCause());
		assertEquals(-1, e.getIndex());
		assertNull(e.getOption());
		assertEquals(parser, e.getParser());
	}

	@Test
	public void construct_UnknownOptionException() {
		String flag = "--flag";

		UnknownOptionException e = new UnknownOptionException(parser, flag);
		assertEquals(flag, e.getFlag());
		assertNull(e.getCause());
		assertEquals(-1, e.getIndex());
		assertNull(e.getOption());
		assertEquals(parser, e.getParser());
	}

	@Test
	public void construct_MissingValueException() {
		Option opt = new Option("opt", null, false, "-o");

		MissingValueException e = new MissingValueException(parser, opt);
		assertNull(e.getCause());
		assertEquals(-1, e.getIndex());
		assertEquals(opt, e.getOption());
		assertEquals(parser, e.getParser());
	}

}
