package com.whirvex.args;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.junit.Test;

/**
 * The unit tests for {@link Args} and related components.
 */
public class ArgsTest {

	@Test
	public void from() {
		assertThrows(NullPointerException.class, () -> Args.from(null));
		String[] args = new String[3];
		for (int i = 0; i < args.length; i++) {
			args[i] = "" + i;
		}

		Args from = Args.from(args);
		List<String> argsList = from.args();
		assertEquals(argsList.size(), args.length);
		for (int i = 0; i < argsList.size(); i++) {
			assertEquals(argsList.get(i), args[i]);
		}
	}

	@Test
	public void construct() {
		assertThrows(NullPointerException.class,
				() -> new Args(Collections.emptyList(), null));
		assertThrows(NullPointerException.class,
				() -> new Args(null, Collections.emptyMap()));

		List<String> argsList = new ArrayList<>();
		Map<Option, List<String>> optsMap = new HashMap<>();

		Args args = new Args(argsList, optsMap);
		assertEquals(args.args(), argsList);
		assertEquals(args.opts(), optsMap.keySet());
	}

	@Test
	public void args() {
		List<String> argsList = new ArrayList<>();
		for (int i = 0; i < 3; i++) {
			argsList.add("" + i);
		}
		Args args = new Args(argsList, Collections.emptyMap());
		assertThrows(IndexOutOfBoundsException.class, () -> args.get(-1));
		assertThrows(IndexOutOfBoundsException.class,
				() -> args.get(argsList.size()));

		List<String> internalArgs = args.args();
		assertEquals(internalArgs, argsList);
		assertThrows(UnsupportedOperationException.class,
				() -> internalArgs.clear());

		assertEquals(args.argc(), argsList.size());
		for (int i = 0; i < args.argc(); i++) {
			assertEquals(args.get(i), argsList.get(i));
		}
	}

	@Test
	public void opts() {
		Map<Option, List<String>> optsList = new HashMap<>();
		for (int i = 0; i < 3; i++) {
			Option opt = new Option("opt" + i, null, true, "-" + i);
			optsList.put(opt, Arrays.asList("" + i));
		}
		Args args = new Args(Collections.emptyList(), optsList);
		assertThrows(NullPointerException.class, () -> args.get(null));
		assertThrows(NullPointerException.class, () -> args.get(null, 0));

		Set<Option> internalOpts = args.opts();
		assertEquals(internalOpts, optsList.keySet());
		assertThrows(UnsupportedOperationException.class,
				() -> internalOpts.clear());

		assertEquals(0, args.optc(null));
		assertEquals(0, args.namec(null));
		assertEquals(0, args.flagc(null));

		assertFalse(args.has((Option) null));
		assertFalse(args.hasName(null));
		assertFalse(args.hasFlag(null));

		assertTrue(args.has());
		assertTrue(args.hasNames());
		assertTrue(args.hasFlags());

		Option[] optsArr = new Option[optsList.size()];
		String[] optNames = new String[optsArr.length];
		String[] optFlags = new String[optsArr.length];
		
		assertFalse(args.has(optsArr));
		assertFalse(args.hasNames(optNames));
		assertFalse(args.hasFlags(optFlags));
		
		Iterator<Option> optsI = optsList.keySet().iterator();
		for (int i = 0; i < optsArr.length; i++) {
			optsArr[i] = optsI.next();
			optNames[i] = optsArr[i].name;
			optFlags[i] = optsArr[i].flags[0];
		}

		assertTrue(args.has(optsArr));
		assertTrue(args.hasNames(optNames));
		assertTrue(args.hasFlags(optFlags));

		assertEquals(args.optc(), optsList.size());
		for (Option option : optsList.keySet()) {
			List<String> values = optsList.get(option);

			assertTrue(args.has(option));
			assertTrue(args.hasName(option.name));
			assertTrue(args.hasFlag(option.flags[0]));

			assertEquals(args.optc(option), values.size());
			assertEquals(args.namec(option.name), values.size());
			assertEquals(args.flagc(option.flags[0]), values.size());

			assertEquals(args.get(option, 0), values.get(0));
			assertEquals(args.getName(option.name, 0), values.get(0));
			assertEquals(args.getFlag(option.flags[0], 0), values.get(0));
		}
	}

}
