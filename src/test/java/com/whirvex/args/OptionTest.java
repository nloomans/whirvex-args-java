package com.whirvex.args;

import static org.junit.Assert.*;

import org.junit.Test;

/**
 * The unit tests for {@link Option} and related components.
 */
public class OptionTest {

	@Test
	public void construct() {
		assertThrows(NullPointerException.class,
				() -> new Option(null, null, false));

		Option opt = new Option("opt", "desc", true, "-k", "--key");
		assertEquals("opt", opt.name);
		assertEquals("desc", opt.desc);
		assertTrue(opt.hasValue);

		assertEquals(2, opt.flags.length);
		assertEquals("-k", opt.flags[0]);
		assertEquals("--key", opt.flags[1]);
	}

	@Test
	public void usesFlag() {
		Option opt = new Option("opt", null, false);
		assertFalse(opt.usesFlag(null));
		assertFalse(opt.usesFlag("-k"));

		opt.flags = null;
		assertFalse(opt.usesFlag(null));
		assertFalse(opt.usesFlag("-k"));

		opt.flags = new String[] { "-k", "--key" };
		assertFalse(opt.usesFlag(null));
		assertTrue(opt.usesFlag("--key"));
	}

	@Test
	public void getDisplayName() {
		Option opt = new Option("opt", null, false);
		assertEquals("", opt.getDisplayName());

		opt.flags = null;
		assertEquals(opt.name, opt.getDisplayName());
		opt.flags = new String[] { "-k" };
		assertEquals("-k", opt.getDisplayName());
		opt.flags = new String[] { "-k", "--key" };
		assertEquals("-k, --key", opt.getDisplayName());
	}

	@Test
	public void find() {
		Option[] opts = new Option[4];
		for (int i = 0; i < opts.length; i++) {
			opts[i] = new Option("" + i, null, false, "-" + i);
		}
		opts[opts.length - 1].flags = null;

		for (int i = 0; i < opts.length - 1; i++) {
			assertEquals(opts[i], Option.findName(opts, "" + i));
			assertEquals(opts[i], Option.findFlag(opts, "-" + i));
		}
		assertNull(Option.findName(opts, "" + opts.length));
		assertNull(Option.findFlag(opts, "-" + opts.length));
	}

}
