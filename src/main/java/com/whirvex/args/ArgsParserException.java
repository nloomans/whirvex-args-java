package com.whirvex.args;

/**
 * Signals that an error has occurred in an {@link ArgsParser} while parsing
 * argument values.
 */
public class ArgsParserException extends ArgsException {

	private static final long serialVersionUID = 4144722874026264865L;

	private final ArgsParser parser;

	/**
	 * Constructs a new {@code ArgsParserException} with the specified detail
	 * message and cause.
	 * <p>
	 * Note that the detail message associated with {@code cause} is <i>not</i>
	 * automatically incorporated in this exception's detail message.
	 *
	 * @param parser
	 *            the parser that triggered the exception.
	 * @param index
	 *            the index of the invalid argument (which is saved for later
	 *            retrieval by the {@link ArgsParserException#getIndex()}
	 *            method). A value less than {@code 0} is permitted and clamped
	 *            to {@code -1}. This indicates that the index of the argument
	 *            is unknown, or the exception was not caused by a positional
	 *            argument.
	 * @param message
	 *            the detail message (which is saved for later retrieval by the
	 *            {@link Throwable#getMessage()} method).
	 * @param cause
	 *            the cause (which is saved for later retrieval by the
	 *            {@link Throwable#getCause()} method). A {@code null} value is
	 *            permitted, and indicates that the cause is nonexistent or
	 *            unknown.
	 */
	public ArgsParserException(ArgsParser parser, int index, String message,
			Throwable cause) {
		super(index, message, cause);
		this.parser = parser;
	}

	/**
	 * Constructs a new {@code ArgsParserException} with the specified detail
	 * message.
	 *
	 * @param parser
	 *            the parser that triggered the exception.
	 * @param index
	 *            the index of the invalid argument (which is saved for later
	 *            retrieval by the {@link ArgsParserException#getIndex()}
	 *            method). A value less than {@code 0} is permitted and clamped
	 *            to {@code -1}. This indicates that the index of the argument
	 *            is unknown, or the exception was not caused by a positional
	 *            argument.
	 * @param message
	 *            the detail message (which is saved for later retrieval by the
	 *            {@link Throwable#getMessage()} method).
	 */
	public ArgsParserException(ArgsParser parser, int index, String message) {
		this(parser, index, message, null);
	}

	/**
	 * Constructs a new {@code ArgsParserException} with the specified cause.
	 * <p>
	 * Note that the detail message associated with {@code cause} is <i>not</i>
	 * automatically incorporated in this exception's detail message.
	 *
	 * @param parser
	 *            the parser that triggered the exception.
	 * @param index
	 *            the index of the invalid argument (which is saved for later
	 *            retrieval by the {@link ArgsParserException#getIndex()}
	 *            method). A value less than {@code 0} is permitted and clamped
	 *            to {@code -1}. This indicates that the index of the argument
	 *            is unknown, or the exception was not caused by a positional
	 *            argument.
	 * @param cause
	 *            the cause (which is saved for later retrieval by the
	 *            {@link Throwable#getCause()} method). A {@code null} value is
	 *            permitted, and indicates that the cause is nonexistent or
	 *            unknown.
	 */
	public ArgsParserException(ArgsParser parser, int index, Throwable cause) {
		this(parser, index, null, cause);
	}

	/**
	 * Constructs a new {@code ArgsParserException} with no detail message.
	 *
	 * @param parser
	 *            the parser that triggered the exception.
	 * @param index
	 *            the index of the invalid argument (which is saved for later
	 *            retrieval by the {@link ArgsParserException#getIndex()}
	 *            method). A value less than {@code 0} is permitted and clamped
	 *            to {@code -1}. This indicates that the index of the argument
	 *            is unknown, or the exception was not caused by a positional
	 *            argument.
	 */
	public ArgsParserException(ArgsParser parser, int index) {
		this(parser, index, null, null);
	}

	/**
	 * Constructs a new {@code ArgsParserException} with the specified detail
	 * message and cause.
	 * <p>
	 * Note that the detail message associated with {@code cause} is <i>not</i>
	 * automatically incorporated in this exception's detail message.
	 *
	 * @param parser
	 *            the parser that triggered the exception.
	 * @param opt
	 *            the option of the invalid argument (which is saved for later
	 *            retrieval by the {@link ArgsParserException#getOption()}
	 *            method). A {@code null} value is permitted. This indicates
	 *            that the option of the argument is unknown, or the exception
	 *            was not caused by a option.
	 * @param message
	 *            the detail message (which is saved for later retrieval by the
	 *            {@link Throwable#getMessage()} method).
	 * @param cause
	 *            the cause (which is saved for later retrieval by the
	 *            {@link Throwable#getCause()} method). A {@code null} value is
	 *            permitted, and indicates that the cause is nonexistent or
	 *            unknown.
	 */
	public ArgsParserException(ArgsParser parser, Option opt, String message,
			Throwable cause) {
		super(opt, message, cause);
		this.parser = parser;
	}

	/**
	 * Constructs a new {@code ArgsParserException} with the specified detail
	 * message.
	 *
	 * @param parser
	 *            the parser that triggered the exception.
	 * @param opt
	 *            the option of the invalid argument (which is saved for later
	 *            retrieval by the {@link ArgsParserException#getOption()}
	 *            method). A {@code null} value is permitted. This indicates
	 *            that the option of the argument is unknown, or the exception
	 *            was not caused by a option.
	 * @param message
	 *            the detail message (which is saved for later retrieval by the
	 *            {@link Throwable#getMessage()} method).
	 */
	public ArgsParserException(ArgsParser parser, Option opt, String message) {
		this(parser, opt, message, null);
	}

	/**
	 * Constructs a new {@code ArgsParserException} with the specified cause.
	 * <p>
	 * Note that the detail message associated with {@code cause} is <i>not</i>
	 * automatically incorporated in this exception's detail message.
	 *
	 * @param parser
	 *            the parser that triggered the exception.
	 * @param opt
	 *            the option of the invalid argument (which is saved for later
	 *            retrieval by the {@link ArgsParserException#getOption()}
	 *            method). A {@code null} value is permitted. This indicates
	 *            that the option of the argument is unknown, or the exception
	 *            was not caused by a option.
	 * @param cause
	 *            the cause (which is saved for later retrieval by the
	 *            {@link Throwable#getCause()} method). A {@code null} value is
	 *            permitted, and indicates that the cause is nonexistent or
	 *            unknown.
	 */
	public ArgsParserException(ArgsParser parser, Option opt, Throwable cause) {
		this(parser, opt, null, cause);
	}

	/**
	 * Constructs a new {@code ArgsParserException} with no detail message.
	 *
	 * @param parser
	 *            the parser that triggered the exception.
	 * @param opt
	 *            the option of the invalid argument (which is saved for later
	 *            retrieval by the {@link ArgsParserException#getOption()}
	 *            method). A {@code null} value is permitted. This indicates
	 *            that the option of the argument is unknown, or the exception
	 *            was not caused by a option.
	 */
	public ArgsParserException(ArgsParser parser, Option opt) {
		this(parser, opt, null, null);
	}

	/**
	 * Constructs a new {@code ArgsParserException} with the specified detail
	 * message and cause.
	 * <p>
	 * Note that the detail message associated with {@code cause} is <i>not</i>
	 * automatically incorporated in this exception's detail message.
	 *
	 * @param parser
	 *            the parser that triggered the exception.
	 * @param message
	 *            the detail message (which is saved for later retrieval by the
	 *            {@link Throwable#getMessage()} method).
	 * @param cause
	 *            the cause (which is saved for later retrieval by the
	 *            {@link Throwable#getCause()} method). A {@code null} value is
	 *            permitted, and indicates that the cause is nonexistent or
	 *            unknown.
	 */
	public ArgsParserException(ArgsParser parser, String message,
			Throwable cause) {
		super(message, cause);
		this.parser = parser;
	}

	/**
	 * Constructs a new {@code ArgsParserException} with the specified detail
	 * message.
	 *
	 * @param parser
	 *            the parser that triggered the exception.
	 * @param message
	 *            the detail message (which is saved for later retrieval by the
	 *            {@link Throwable#getMessage()} method).
	 */
	public ArgsParserException(ArgsParser parser, String message) {
		this(parser, message, (Throwable) null);
	}

	/**
	 * Constructs a new {@code ArgsParserException} with the specified cause.
	 * <p>
	 * Note that the detail message associated with {@code cause} is <i>not</i>
	 * automatically incorporated in this exception's detail message.
	 * 
	 * @param parser
	 *            the parser that triggered the exception.
	 * @param cause
	 *            the cause (which is saved for later retrieval by the
	 *            {@link Throwable#getCause()} method). A {@code null} value is
	 *            permitted, and indicates that the cause is nonexistent or
	 *            unknown.
	 */
	public ArgsParserException(ArgsParser parser, Throwable cause) {
		this(parser, (String) null, cause);
	}

	/**
	 * Constructs a new {@code ArgsParserException} with no detail message.
	 * 
	 * @param parser
	 *            the parser that triggered the exception.
	 */
	public ArgsParserException(ArgsParser parser) {
		this(parser, (String) null, (Throwable) null);
	}

	/**
	 * Constructs a new {@code ArgsParserException} with no detail message.
	 */
	public ArgsParserException() {
		this(null, (String) null, (Throwable) null);
	}

	/**
	 * Returns the parser that triggered the exception.
	 * 
	 * @return the parser that triggered the exception, may be {@code null}.
	 */
	public ArgsParser getParser() {
		return this.parser;
	}

}
