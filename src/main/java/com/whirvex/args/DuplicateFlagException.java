package com.whirvex.args;

/**
 * Signals that an error has occured in parsing due to multiple options sharing
 * identical flags.
 */
public class DuplicateFlagException extends ArgsParserException {

	private static final long serialVersionUID = 3138953320207892605L;
	
	private final String flag;
	
	/**
	 * Constructs a new {@code DuplicateFlagException}.
	 * 
	 * @param parser
	 *            the parser that triggered the exception.
	 * @param flag
	 *            the duplicate flag.
	 */
	public DuplicateFlagException(ArgsParser parser, String flag) {
		super(parser, "duplicate option flag \"" + flag + "\"");
		this.flag = flag;
	}
	
	/**
	 * Returns the duplicate option flag.
	 * 
	 * @return the duplicate flag.
	 */
	public String getFlag() {
		return this.flag;
	}

}
