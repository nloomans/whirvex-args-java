package com.whirvex.args;

import java.util.Objects;
import java.util.function.Predicate;

/**
 * Enforces rules for a set of arguments. When a requirement is not met,
 * execution is halted by throwing an {@link ArgsException}.
 */
public class Require {

	public final Args args;

	/**
	 * Constructs requirements for a set of arguments.
	 * 
	 * @param args
	 *            the arguments.
	 * @throws NullPointerException
	 *             if {@code args} is {@code null}.
	 */
	public Require(Args args) {
		this.args = Objects.requireNonNull(args, "args");
	}

	/**
	 * Requires a condition be satisfied.
	 * 
	 * @param predicate
	 *            the condition to be satisfied.
	 * @param msg
	 *            the message to display if {@code predicate} is unsatisfied,
	 *            may be {@code null} for no message.
	 * @return the requirements.
	 * @throws ArgsException
	 *             if {@code predicate} returns {@code false} when tested.
	 */
	public Require cond(Predicate<Args> predicate, String msg) {
		if (!predicate.test(args)) {
			throw new ArgsException(msg != null ? msg : "");
		}
		return this;
	}

	/**
	 * Requires a condition be satisfied.
	 * 
	 * @param predicate
	 *            the condition to be satisfied.
	 * @return the requirements.
	 * @throws ArgsException
	 *             if {@code predicate} returns {@code false} when tested.
	 */
	public Require cond(Predicate<Args> predicate) {
		return this.cond(predicate, null);
	}

	/**
	 * Requires a minimum amount of arguments be present.
	 * <p>
	 * Shall less than {@code count} arguments be present, the exception detail
	 * message shall be {@code count + " arguments required"}.
	 * 
	 * @param count
	 *            the minimum amount of arguments.
	 * @return the requirements.
	 * @throws ArgsException
	 *             if {@code args.argc()} returns a value less than
	 *             {@code count}.
	 */
	public Require argc(int count) {
		if (args.argc() < count) {
			throw new ArgsException(count + " arguments required");
		}
		return this;
	}

	/**
	 * Requires a minimum amount of option be present.
	 * <p>
	 * Shall less than {@code count} option be present, the exception detail
	 * message shall be {@code count + " options required"}.
	 * 
	 * @param count
	 *            the minimum amount of options.
	 * @return the requirements.
	 * @throws ArgsException
	 *             if {@code args.optc()} returns a value less than
	 *             {@code count}.
	 */
	public Require optc(int count) {
		if (args.optc() < count) {
			throw new ArgsException(count + " options required");
		}
		return this;
	}

	/**
	 * Requires a minimum amount of occurrences for an option.
	 * <p>
	 * Shall there be less than {@code count} occurrences, the exception detail
	 * message shall be {@code count + " occurrences required for "
	 * + opt.getDisplayName()}.
	 * 
	 * @param opt
	 *            the option.
	 * @param count
	 *            the minimum amount of occurrences.
	 * @return the requirements.
	 * @throws ArgsException
	 *             if {@code args.optc(opt)} returns a value less than
	 *             {@code count}.
	 */
	public Require optc(Option opt, int count) {
		if (args.optc(opt) < count) {
			throw new ArgsException(count + " occurrences required for "
					+ opt.getDisplayName());
		}
		return this;
	}

	/**
	 * Requires a minimum amount of occurrences for an option with {@code name}.
	 * <p>
	 * Shall there be less than {@code count} occurrences, the exception detail
	 * message shall be {@code count + " occurrences required for" +
	 * " option with name \"" + name + "\""}.
	 * 
	 * @param name
	 *            the option name.
	 * @param count
	 *            the minimum amount of occurrences.
	 * @return the requirements.
	 * @throws ArgsException
	 *             if {@code args.namec(opt)} returns a value less than
	 *             {@code count}.
	 */
	public Require namec(String name, int count) {
		if (args.namec(name) < count) {
			throw new ArgsException(count + " occurrences required for"
					+ " option with name \"" + name + "\"");
		}
		return this;
	}

	/**
	 * Requires a minimum amount of occurrences for an option using
	 * {@code flag}.
	 * <p>
	 * Shall there be less than {@code count} occurrences, the exception detail
	 * message shall be {@code count + " occurrences required for" + 
	 * " option using flag \"" + flag + "\""}.
	 * 
	 * @param flag
	 *            the option flag.
	 * @param count
	 *            the minimum amount of occurrences.
	 * @return the requirements.
	 * @throws ArgsException
	 *             if {@code args.flagc(opt)} returns a value less than
	 *             {@code count}.
	 */
	public Require flagc(String flag, int count) {
		if (args.flagc(flag) < count) {
			throw new ArgsException(count + " occurrences required for"
					+ " option using flag \"" + flag + "\"");
		}
		return this;
	}

	/**
	 * Requires a set of options be present.
	 * <p>
	 * Shall an option not be present, the exception detail message shall be
	 * {@code "missing required option " + opt.getDisplayName()}.
	 * 
	 * @param opts
	 *            the options.
	 * @return the requirements.
	 * @throws ArgsException
	 *             if {@code args.has(opt)} returns {@code false} for an option.
	 */
	public Require has(Option... opts) {
		for (Option opt : opts) {
			if (!args.has(opt)) {
				throw new ArgsException(
						"missing required option " + opt.getDisplayName());
			}
		}
		return this;
	}

	/**
	 * Requires a set of options to be present.
	 * <p>
	 * Shall an option not be present, the exception detail message shall be
	 * {@code "missing required option" + " with name \"" + name + "\""}.
	 * 
	 * @param names
	 *            the option names.
	 * @return the requirements.
	 * @throws ArgsException
	 *             if {@code args.hasName(name)} returns {@code false} for an
	 *             option name.
	 */
	public Require hasName(String... names) {
		for (String name : names) {
			if (!args.hasName(name)) {
				throw new ArgsException("missing required option"
						+ " with name \"" + name + "\"");
			}
		}
		return this;
	}

	/**
	 * Requires a set of options to be present.
	 * <p>
	 * Shall an option not be present, the exception detail message shall be
	 * {@code "missing required option" + " using flag \"" + flag + "\""}.
	 * 
	 * @param flags
	 *            the option flags.
	 * @return the requirements.
	 * @throws ArgsException
	 *             if {@code args.hasFlag(flag)} returns {@code false} for an
	 *             option flag.
	 */
	public Require hasFlag(String... flags) {
		for (String flag : flags) {
			if (!args.hasFlag(flag)) {
				throw new ArgsException("missing required option"
						+ " using flag \"" + flag + "\"");
			}
		}
		return this;
	}

}
