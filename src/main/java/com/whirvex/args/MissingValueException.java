package com.whirvex.args;

/**
 * Signals that an error has occurred in parsing due to a value being
 * unspecified for an option which requires one.
 */
public class MissingValueException extends ArgsParserException {

	private static final long serialVersionUID = 1810940665439600121L;

	/**
	 * Constructs a new {@code MissingValueException}.
	 * 
	 * @param parser
	 *            the parser that triggered the exception.
	 * @param opt
	 *            the option missing its require value.
	 */
	public MissingValueException(ArgsParser parser, Option opt) {
		super(parser, opt, "missing required value");
	}

}
