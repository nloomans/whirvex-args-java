/**
 * A general purpose arguments parser system.
 * 
 * @see com.whirvex.args.Args Args
 * @see com.whirvex.args.ArgsParser ArgsParser
 * @see com.whirvex.args.Option Option
 */
package com.whirvex.args;
