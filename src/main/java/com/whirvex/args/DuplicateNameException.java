package com.whirvex.args;

/**
 * Signals that an error has occured in parsing due to multiple options sharing
 * identical names.
 */
public class DuplicateNameException extends ArgsParserException {

	private static final long serialVersionUID = -8292663925691172640L;

	private final String name;

	/**
	 * Constructs a new {@code DuplicateNameException}.
	 * 
	 * @param parser
	 *            the parser that triggered the exception.
	 * @param name
	 *            the duplicate name.
	 */
	public DuplicateNameException(ArgsParser parser, String name) {
		super(parser, "duplicate option name \"" + name + "\"");
		this.name = name;
	}

	/**
	 * Returns the duplicate option name.
	 * 
	 * @return the duplicate name.
	 */
	public String getName() {
		return this.name;
	}

}
