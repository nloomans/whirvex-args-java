package com.whirvex.args;

/**
 * Signals that an option is using an invalid flag for arguments parsing.
 */
public class IllegalFlagException extends IllegalArgumentException {

	private static final long serialVersionUID = 710427087156290781L;

	/**
	 * Constructs a new {@code IllegalArgumentException} with the specified
	 * detail message.
	 *
	 * @param message
	 *            the detail message.
	 */
	public IllegalFlagException(String message) {
		super(message);
	}

}
