package com.whirvex.args;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

/**
 * A container for parsed arguments.
 * 
 * @see Option
 */
public class Args {

	private static final ArgsParser STD_PARSER = new StdArgsParser();

	/**
	 * Constructs a new {@code Args} container for an arguments array.
	 * <p>
	 * <b>Note</b>: This only creates a container for {@code args}, no parsing
	 * is performed. As a result, options will not be accounted for. To have
	 * proper argument and option parsing for an arguments array, use
	 * {@link #parse(String[], Option...)}.
	 * 
	 * @param args
	 *            the arguments.
	 * @return the arguments container.
	 * @throws NullPointerException
	 *             if {@code args} is {@code null}.
	 */
	public static Args from(String[] args) {
		return new Args(Arrays.asList(args), Collections.emptyMap());
	}

	/**
	 * Parses an array of arguments for a set of options using the standard
	 * arguments parser.
	 * 
	 * @param args
	 *            the argument values.
	 * @param opts
	 *            the supported options, if any.
	 * @return the arguments container.
	 * @throws NullPointerException
	 *             if {@code args} or {@code opts} are {@code null}.
	 * @see StdArgsParser
	 */
	public static Args parse(String[] args, Option... opts) {
		return STD_PARSER.parse(args, opts);
	}

	public final Require require;
	private final List<String> args;
	private final Map<Option, List<String>> opts;

	/**
	 * Constructs a new {@code Args} container for the specified arguments list
	 * and options map.
	 * 
	 * @param args
	 *            the arguments.
	 * @param opts
	 *            the options.
	 * @throws NullPointerException
	 *             if {@code args} or {@code opts} are {@code null}.
	 */
	public Args(List<String> args, Map<Option, List<String>> opts) {
		this.require = new Require(this);
		this.args = Objects.requireNonNull(args, "args");
		this.opts = Objects.requireNonNull(opts, "opts");
	}

	/**
	 * Returns the positional arguments.
	 * 
	 * @return the positional arguments.
	 */
	public List<String> args() {
		return Collections.unmodifiableList(args);
	}

	/**
	 * Returns the amount of positional arguments.
	 * 
	 * @return the positional argument count.
	 */
	public int argc() {
		return args.size();
	}

	/**
	 * Returns all present options.
	 * 
	 * @return all present options.
	 */
	public Set<Option> opts() {
		return Collections.unmodifiableSet(opts.keySet());
	}

	/**
	 * Returns the amount of present options.
	 * 
	 * @return the present option count.
	 */
	public int optc() {
		return opts.size();
	}

	/**
	 * Returns the amount of occurrences for an option.
	 * 
	 * @param opt
	 *            the option.
	 * @return the occurrence count of {@code opt}.
	 */
	public int optc(Option opt) {
		List<?> values = opts.get(opt);
		return values != null ? values.size() : 0;
	}

	/**
	 * Searches for an option with a given name in this set of arguments and
	 * returns it after being found. If two or more options have an identical
	 * name, the first one encountered will be returned.
	 * 
	 * @param name
	 *            the option name.
	 * @return the first option found having a name identical to {@code name}.
	 */
	public Option findName(String name) {
		return Option.findName(opts.keySet(), name);
	}

	/**
	 * Returns the amount of occurrences for an option with a given name.
	 * 
	 * @param name
	 *            the option name.
	 * @return the occurrence count for option with {@code name}.
	 */
	public int namec(String name) {
		Option opt = this.findName(name);
		return opt != null ? this.optc(opt) : 0;
	}

	/**
	 * Searches for an option with a given flag in this set of arguments and
	 * returns it after being found. If two or more options use an identical
	 * flag, the first one encountered will be returned.
	 * 
	 * @param flag
	 *            the option flag.
	 * @return the first option found using a flag identical to {@code flag}.
	 */
	public Option findFlag(String flag) {
		return Option.findFlag(opts.keySet(), flag);
	}

	/**
	 * Returns the amount of occurrences for an option using a given flag.
	 * 
	 * @param flag
	 *            the option flag.
	 * @return the occurrence count for option using {@code flag}.
	 */
	public int flagc(String flag) {
		Option opt = this.findFlag(flag);
		return opt != null ? this.optc(opt) : 0;
	}

	/**
	 * Returns an argument at a given index.
	 * 
	 * @param index
	 *            the index.
	 * @return the value or argument at {@code index}.
	 * @throws IndexOutOfBoundsException
	 *             if {@code index} is out of range for {@code args}.
	 */
	public String get(int index) {
		return args.get(index);
	}

	/**
	 * Returns if an option is present.
	 * 
	 * @param opt
	 *            the option.
	 * @return {@code true} if {@code opt} is present, {@code false} otherwise.
	 */
	public boolean has(Option opt) {
		return opts.containsKey(opt);
	}

	/**
	 * Returns if all options in an iterable are present.
	 * 
	 * @param opts
	 *            the options.
	 * @return {@code true} if all given options are present, {@code false}
	 *         otherwise.
	 */
	public boolean has(Iterable<Option> opts) {
		for (Option opt : opts) {
			if (!this.has(opt)) {
				return false;
			}
		}
		return true;
	}

	/**
	 * Returns if all given options are present.
	 * 
	 * @param opts
	 *            the options.
	 * @return {@code true} if all given options are present, {@code false}
	 *         otherwise.
	 */
	public boolean has(Option... opts) {
		return this.has(Arrays.asList(opts));
	}

	/**
	 * Returns the given values for an option.
	 * 
	 * @param opt
	 *            the option.
	 * @return the values for {@code opt}.
	 * @throws NullPointerException
	 *             if {@code opt} is not present.
	 */
	public List<String> get(Option opt) {
		List<String> values = opts.get(opt);
		if (values == null) {
			throw new NullPointerException("opt not present");
		}
		return Collections.unmodifiableList(values);
	}

	/**
	 * Returns a given value for an option at a specified index.
	 * 
	 * @param opt
	 *            the option.
	 * @param index
	 *            the index of the value.
	 * @return the option value at {@code index}.
	 * @throws NullPointerException
	 *             if {@code opt} is not present.
	 * @throws IndexOutOfBoundsException
	 *             if {@code index} is out of range for the option's values.
	 */
	public String get(Option opt, int index) {
		List<String> values = this.get(opt);
		return values.get(index);
	}

	/**
	 * Returns if an option with a given name is present.
	 * 
	 * @param name
	 *            the option name.
	 * @return {@code true} if an option with {@code name} is present,
	 *         {@code false} otherwise.
	 */
	public boolean hasName(String name) {
		return this.findName(name) != null;
	}

	/**
	 * Returns if all options with the names in an iterable are present.
	 * 
	 * @param names
	 *            the option names.
	 * @return {@code true} if all options with the given names are present,
	 *         {@code false} otherwise.
	 */
	public boolean hasNames(Iterable<String> names) {
		for (String name : names) {
			if (!this.hasName(name)) {
				return false;
			}
		}
		return true;
	}

	/**
	 * Returns if all options with the given names are present.
	 * 
	 * @param names
	 *            the option names.
	 * @return {@code true} if all options with the given names are present,
	 *         {@code false} otherwise.
	 */
	public boolean hasNames(String... names) {
		return this.hasNames(Arrays.asList(names));
	}

	/**
	 * Returns the given values for an option.
	 * 
	 * @param name
	 *            the option name.
	 * @return the values for option with {@code name}.
	 * @throws NullPointerException
	 *             if no option with {@code name} is present.
	 */
	public List<String> getName(String name) {
		Option opt = this.findName(name);
		return this.get(opt);
	}

	/**
	 * Returns a given value for an option at a specified index.
	 * 
	 * @param name
	 *            the option name.
	 * @param index
	 *            the index of the value.
	 * @return the option value at {@code index}.
	 * @throws NullPointerException
	 *             if no option with {@code name} is present.
	 * @throws IndexOutOfBoundsException
	 *             if {@code index} is out of range for the option's values.
	 */
	public String getName(String name, int index) {
		List<String> values = this.getName(name);
		return values.get(index);
	}

	/**
	 * Returns if an option using a given flag is present.
	 * 
	 * @param flag
	 *            the option flag.
	 * @return {@code true} if an option using {@code flag} is present,
	 *         {@code false} otherwise.
	 */
	public boolean hasFlag(String flag) {
		return this.findFlag(flag) != null;
	}

	/**
	 * Returns if all options using the flags in an iterable are present.
	 * 
	 * @param flags
	 *            the option flags.
	 * @return {@code true} if all options using the given flags are present,
	 *         {@code false} otherwise.
	 */
	public boolean hasFlags(Iterable<String> flags) {
		for (String flag : flags) {
			if (!this.hasFlag(flag)) {
				return false;
			}
		}
		return true;
	}

	/**
	 * Returns if all options using the given flags are present.
	 * 
	 * @param flags
	 *            the option flags.
	 * @return {@code true} if all options using the given flags are present,
	 *         {@code false} otherwise.
	 */
	public boolean hasFlags(String... flags) {
		return this.hasFlags(Arrays.asList(flags));
	}

	/**
	 * Returns the given values for an option.
	 * 
	 * @param flag
	 *            the option flag.
	 * @return the values for option using {@code flag}.
	 * @throws NullPointerException
	 *             if no option using {@code flag} is present.
	 */
	public List<String> getFlag(String flag) {
		Option opt = this.findFlag(flag);
		return this.get(opt);
	}

	/**
	 * Returns a given value for an option at a specified index.
	 * 
	 * @param flag
	 *            the option flag.
	 * @param index
	 *            the index of the value.
	 * @return the option value at {@code index}.
	 * @throws NullPointerException
	 *             if no option using {@code flag} is present.
	 * @throws IndexOutOfBoundsException
	 *             if {@code index} is out of range for the option's values.
	 */
	public String getFlag(String flag, int index) {
		List<String> values = this.getFlag(flag);
		return values.get(index);
	}

}
