package com.whirvex.args;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;

/**
 * Parses an array of arguments to an {@link Args} container.
 */
@FunctionalInterface
public interface ArgsParser {

	/**
	 * Parses a set of arguments.
	 * 
	 * @param args
	 *            the argument values.
	 * @param opts
	 *            the options.
	 * @return the parsed arguments values.
	 * @throws NullPointerException
	 *             if {@code args} or {@code opts} are {@code null}.
	 * @throws ArgsParserException
	 *             if an error occurs during parsing.
	 */
	public Args parse(Iterable<String> args, Set<Option> opts);

	/**
	 * Parses a set of arguments.
	 * <p>
	 * This function is a shorthand for {@link #parse(String[], Set)}, with the
	 * {@code args} parameter being converted from an array into a {@code List},
	 * and the {@code opts} parameter being converted from an array into a
	 * {@code Set}.
	 * 
	 * @param args
	 *            the argument values.
	 * @param opts
	 *            the options.
	 * @return the parsed arguments values.
	 * @throws NullPointerException
	 *             if {@code args} or {@code opts} are {@code null}.
	 * @throws ArgsParserException
	 *             if an error occurs during parsing.
	 */
	public default Args parse(String[] args, Option... opts) {
		Objects.requireNonNull(args, "args");
		Objects.requireNonNull(opts, "opts");

		List<String> argsList = Arrays.asList(args);
		Set<Option> optSet = new HashSet<>();
		for (Option opt : opts) {
			optSet.add(opt);
		}
		return this.parse(argsList, optSet);
	}

}
