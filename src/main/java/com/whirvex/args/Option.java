package com.whirvex.args;

import java.util.Arrays;
import java.util.Objects;

/**
 * A named argument which may require a value.
 * 
 * @see Args
 */
public class Option {

	/**
	 * Searches for an option with a given name and returns it after being found
	 * among an option iterable. If two or more options have an identical name,
	 * the first one encountered will be returned.
	 * 
	 * @param opts
	 *            the options to search through.
	 * @param name
	 *            the option name.
	 * @return the first option found having a name identical to {@code name}.
	 */
	public static Option findName(Iterable<Option> opts, String name) {
		for (Option opt : opts) {
			if (Objects.equals(opt.name, name)) {
				return opt;
			}
		}
		return null;
	}

	/**
	 * Searches for an option with a given name and returns it after being found
	 * among an option iterable. If two or more options have an identical name,
	 * the first one encountered will be returned.
	 * <p>
	 * This method is a shorthand for {@link #findName(Iterable, String)}, with
	 * {@code opts} being converted to a list.
	 * 
	 * @param opts
	 *            the options to search through.
	 * @param name
	 *            the option name.
	 * @return the first option found having a name identical to {@code name}.
	 */
	public static Option findName(Option[] opts, String name) {
		return findName(Arrays.asList(opts), name);
	}

	/**
	 * Searches for an option with a given flag and returns it after being found
	 * among an option iterable. If two or more options use an identical flag,
	 * the first one encountered will be returned.
	 * 
	 * @param opts
	 *            the options to search through.
	 * @param flag
	 *            the option flag.
	 * @return the first option found using a flag identical to {@code flag}.
	 */
	public static Option findFlag(Iterable<Option> opts, String flag) {
		for (Option opt : opts) {
			if (opt.usesFlag(flag)) {
				return opt;
			}
		}
		return null;
	}

	/**
	 * Searches for an option with a given flag and returns it after being found
	 * among an option array. If two or more options use an identical flag, the
	 * first one encountered will be returned.
	 * <p>
	 * This method is a shorthand for {@link #findFlag(Iterable, String)}, with
	 * {@code opts} being converted to a list.
	 * 
	 * @param opts
	 *            the options to search through.
	 * @param flag
	 *            the option flag.
	 * @return the first option found using a flag identical to {@code flag}.
	 */
	public static Option findFlag(Option[] opts, String flag) {
		return findFlag(Arrays.asList(opts), flag);
	}

	public final String name;
	public String desc;
	public boolean hasValue;
	public String[] flags;

	/**
	 * Constructs a new {@code Option}.
	 * 
	 * @param name
	 *            the option name, should be unique among other options.
	 * @param desc
	 *            the option description, may be {@code null}.
	 * @param hasValue
	 *            {@code true} if this option requires a value be specified,
	 *            {@code false} otherwise.
	 * @param flags
	 *            the option flags. Specifications and requirements for option
	 *            flags are determined by the {@link ArgsParser} implementation.
	 * @throws NullPointerException
	 *             if {@code name} is {@code null}.
	 */
	public Option(String name, String desc, boolean hasValue, String... flags) {
		this.name = Objects.requireNonNull(name, "name");
		this.desc = desc;
		this.hasValue = hasValue;
		this.flags = flags;
	}

	/**
	 * Returns whether or not this option uses a flag.
	 * 
	 * @param flag
	 *            the option flag.
	 * @return {@code true} if this option uses {@code flag}, {@code false}
	 *         otherwise.
	 */
	public boolean usesFlag(String flag) {
		if (flag == null || flags == null) {
			return false;
		}
		for (String using : flags) {
			if (flag.equals(using)) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Returns the display name of an option.
	 * 
	 * @return the display name of an option, {@code name} if {@code flags} is
	 *         {@code null}.
	 */
	public String getDisplayName() {
		if (flags == null) {
			return this.name;
		}
		return String.join(", ", flags);
	}

}
