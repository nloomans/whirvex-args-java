package com.whirvex.args;

/**
 * Signals that an error has occurred in parsing due to an unknown option flag
 * being encountered.
 */
public class UnknownOptionException extends ArgsParserException {

	private static final long serialVersionUID = 6649348554681510746L;

	private static String getMessage(String flag) {
		String message = "unknown option flag";
		if (flag != null) {
			message += " \"" + flag + "\"";
		}
		return message;
	}

	private final String flag;

	/**
	 * Constructs a new {@code UnknownOptionException}.
	 * 
	 * @param parser
	 *            the parser that triggered the exception.
	 * @param flag
	 *            the unknown option flag.
	 */
	public UnknownOptionException(ArgsParser parser, String flag) {
		super(parser, getMessage(flag));
		this.flag = flag;
	}

	/**
	 * Returns the unknown option flag.
	 * 
	 * @return the unknown option flag, may be {@code null}.
	 */
	public String getFlag() {
		return this.flag;
	}

}
