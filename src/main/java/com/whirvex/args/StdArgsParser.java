package com.whirvex.args;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

/**
 * The standard {@link ArgsParser} implementation.
 */
public class StdArgsParser implements ArgsParser {

	private static final String NOVAL = "";

	private static <K, V> void addValue(K key, V value, Map<K, List<V>> map) {
		if (!map.containsKey(key)) {
			List<V> values = new ArrayList<>();
			values.add(value);
			map.put(key, values);
		} else {
			map.get(key).add(value);
		}
	}

	/**
	 * Returns the index within a string of the first occurrence of a whitespace
	 * character. The test for whether or not a character is whitespace is done
	 * by {@link Character#isWhitespace(char)}.
	 * 
	 * @param str
	 *            the string to search.
	 * @return the index of the first occurrence of a whitespace character in
	 *         the character sequence of {@code str}, or {@code -1} if no
	 *         whitespace character occurs.
	 */
	private static int indexOfWhitespace(String str) {
		for (int i = 0; i < str.length(); i++) {
			char c = str.charAt(i);
			if (Character.isWhitespace(c)) {
				return i;
			}
		}
		return -1;
	}

	/**
	 * Calculates the length of the flag in a given flag and value argument.
	 * <p>
	 * This function should be used in conjunction with
	 * {@link #calcValueIndex(String)} for options who have both their flag and
	 * value cacooned into the same string.
	 * 
	 * @param flagValue
	 *            the flag and value string.
	 * @return the flag length.
	 */
	private static int calcFlagLength(String flagValue) {
		for (int i = 0; i < flagValue.length(); i++) {
			char c = flagValue.charAt(i);
			if (Character.isWhitespace(c) || c == '=') {
				return i;
			}
		}
		return flagValue.length();
	}

	/**
	 * Returns the value index for a given flag and value string.
	 * <p>
	 * This function should be used in conjunction with
	 * {@link #calcFlagLength(String)} for options who have both their flag and
	 * value cacooned into the same string.
	 * <p>
	 * This function checks if either a space or an equals sign are present
	 * within the flag and value string. If neither are present, then no value
	 * is present within the string. If either a space or an equals sign is
	 * specified, the text which comes before it is considered the option flag.
	 * The index of the first non-whitespace character after the option flag
	 * (and that comes after the first equals sign when it is present) is
	 * considered to be the index of the value's first character.
	 * <p>
	 * It is possible that this protocol is activated for an option which
	 * <i>does not</i> take in a value. When this happens, what would have been
	 * the option value should be considered a standalone argument.
	 * 
	 * @param flagValue
	 *            the flag and value string.
	 * @return the index of the value's first character for {@code flagValue},
	 *         {@code -1} if no value is present within the string.
	 */
	private static int calcValueIndex(String flagValue) {
		int spaceIndex = indexOfWhitespace(flagValue);
		int equalsIndex = flagValue.indexOf('=');

		/*
		 * The start index is initially set to the length of the flag and value
		 * string. This is because it is possible neither a space or an equals
		 * sign are present within the flag and value string. If this is the
		 * case, setting the value of the start index to the length of the flag
		 * & value string prevents the following loop from executing. In turn,
		 * -1 is correctly returned for the value index.
		 */
		int startIndex = flagValue.length();
		if (spaceIndex >= 0 || equalsIndex >= 0) {
			if (equalsIndex < 0) {
				startIndex = spaceIndex;
			} else {
				startIndex = equalsIndex + 1;
			}
		}

		/*
		 * The index of this character is where the value resides in the string.
		 * This is also why we get the index of both the first whitespace
		 * character as well as the first equals character. Since the equals
		 * sign is not a whitespace character, it would cause this loop to
		 * prematurely end if we did not start the search *after* its first
		 * occurrence in the string (when it's present).
		 */
		for (int i = startIndex; i < flagValue.length(); i++) {
			char c = flagValue.charAt(i);
			if (!Character.isWhitespace(c)) {
				return i;
			}
		}
		return -1;
	}

	/**
	 * Determines the value of a given flag and value string.
	 * <p>
	 * This function makes use of {@link #calcValueIndex(String)} to determine
	 * where the value for a flag and value string is located. If no value is
	 * present, {@code null} will be returned.
	 * 
	 * @param flagValue
	 *            the flag and value string.
	 * @return the value present within {@code flagValue}, {@code null} if no
	 *         value is present.
	 * @see #calcFlagLength(String)
	 */
	private static String calcValue(String flagValue) {
		int valueIndex = calcValueIndex(flagValue);
		if (valueIndex >= 0) {
			return flagValue.substring(valueIndex, flagValue.length());
		}
		return null;
	}

	private Iterator<String> argsI;
	private Set<Option> opts;

	private boolean parseOpts;
	private List<String> argList;
	private Map<Option, List<String>> optMap;

	private void validateFlag(String flag) {
		if (!flag.startsWith("--") && !flag.startsWith("-")) {
			throw new IllegalFlagException("must start with - or --");
		} else if (!flag.matches("\\S+")) {
			throw new IllegalFlagException("cannot contain whitespace");
		} else if (!flag.startsWith("--") && flag.length() != 2) {
			throw new IllegalFlagException("short flag must be of length 2");
		} else if (flag.startsWith("--") && flag.length() == 2) {
			throw new IllegalFlagException("long flag must have identifier");
		}
	}

	private void validateOptions() {
		List<String> names = new ArrayList<>();
		Map<String, Option> flags = new HashMap<>();
		for (Option opt : opts) {
			/*
			 * If two or more options share duplicate names, then one or more
			 * will become unusable via their name.
			 */
			String name = opt.name;
			if (names.contains(name)) {
				throw new DuplicateNameException(this, name);
			}
			names.add(name);

			/*
			 * If two options share duplicate flags, then one ore more will
			 * become unusable via one or more of their flags.
			 */
			for (String flag : opt.flags) {
				this.validateFlag(flag);
				if (flags.put(flag, opt) != null) {
					throw new DuplicateFlagException(this, flag);
				}
			}
		}
	}

	private void addNextArgAsValue(Option opt) {
		if (!argsI.hasNext()) {
			throw new MissingValueException(this, opt);
		}

		/*
		 * An option's value was specified in the next argument (rather than
		 * cacooned in the same string). Jump to the next argument string in the
		 * array and add it as the option value.
		 */
		String value = argsI.next();
		if (value.equals("--")) /* Disable flag */ {
			throw new MissingValueException(this, opt);
		}
		addValue(opt, value, optMap);

	}

	private void parseLongFlag(String arg) {
		String flag = arg.substring(0, calcFlagLength(arg));
		if (flag.equals("--")) /* Disable flag */ {
			this.parseOpts = false;
			return; // Disabled options
		}

		Option opt = Option.findFlag(opts, flag);
		if (opt == null) {
			throw new UnknownOptionException(this, flag);
		}

		String value = calcValue(arg);
		if (opt.hasValue == true) {
			if (value != null) {
				addValue(opt, value, optMap);
			} else {
				this.addNextArgAsValue(opt);
			}
		} else {
			addValue(opt, NOVAL, optMap);

			/*
			 * The option takes no value, but somehow a value got cacooned with
			 * it. Add it as a positional argument.
			 */
			if (value != null) {
				argList.add(value);
			}
		}
	}

	private void parseShortFlag(String arg) {
		String flags = arg.substring(0, calcFlagLength(arg));
		String value = calcValue(arg);

		for (int i = 1; i < flags.length(); i++) {
			String flag = "-" + flags.charAt(i);
			Option opt = Option.findFlag(opts, flag);
			if (opt == null) {
				throw new UnknownOptionException(this, flag);
			}

			if (opt.hasValue == false) {
				addValue(opt, NOVAL, optMap);
				continue; // No value to calculate
			}

			/*
			 * A space is not required to specify the value of an option when
			 * using a short flag.
			 * 
			 * When there are more characters remaining in the flags string, a
			 * substring of the entire argument from the last flag to the end of
			 * the string is used as the option value. The entire argument
			 * string is used since the flags string may not include the entire
			 * value (some form of whitespace or other characters may have cut
			 * it out of the flags string).
			 */
			if (i + 1 < flags.length()) {
				value = arg.substring(i + 1);
			}

			/*
			 * Break the loop since all characters after this option represent a
			 * value. Nullify value after it is added as an option value so it
			 * is not a mistakened for a cacooned argument.
			 */
			if (value != null) {
				addValue(opt, value, optMap);
				value = null;
			} else {
				this.addNextArgAsValue(opt);
			}
			break;
		}

		/*
		 * None of the options took in a value, but somehow a value got cacooned
		 * with them. Add it as a positional argument.
		 */
		if (value != null) {
			argList.add(value);
		}
	}

	private boolean parseOption(String arg) {
		if (parseOpts == true) {
			if (arg.startsWith("--")) {
				this.parseLongFlag(arg);
				return true;
			} else if (arg.startsWith("-")) {
				this.parseShortFlag(arg);
				return true;
			}
		}
		return false;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @throws NullPointerException
	 *             if {@code args} or {@code opts} are {@code null}.
	 * @throws DuplicateNameException
	 *             if two or more options share an identical name.
	 * @throws IllegalFlagException
	 *             if an option uses a flag that does not start with {@code "-"}
	 *             or {@code "--"}, contains whitespace, is a short flag not of
	 *             length {@code 2}, or is a long flag without an identifier.
	 * @throws DuplicateFlagException
	 *             if two or more options use an identical flag.
	 * @throws UnknownOptionException
	 *             if an unknown option is encountered.
	 * @throws MissingValueException
	 *             if an option that requires a value has none specified.
	 */
	@Override
	public Args parse(Iterable<String> args, Set<Option> opts) {
		this.argsI = Objects.requireNonNull(args, "args").iterator();
		this.opts = Objects.requireNonNull(opts, "opts");
		this.validateOptions();

		this.parseOpts = true;
		this.argList = new ArrayList<>();
		this.optMap = new HashMap<>();

		while (argsI.hasNext()) {
			String arg = argsI.next();
			boolean parsed = this.parseOption(arg);
			if (parsed == false) {
				argList.add(arg);
			}
		}
		return new Args(argList, optMap);
	}

}
