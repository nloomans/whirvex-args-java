package com.whirvex.args;

/**
 * Signals that an invalid argument has been given.
 * 
 * @see Args
 */
public class ArgsException extends IllegalArgumentException {

	private static final long serialVersionUID = -3793765633031918362L;

	private final int index;
	private final Option opt;

	/**
	 * Constructs a new {@code ArgsException} with the specified detail message
	 * and cause.
	 * <p>
	 * Note that the detail message associated with {@code cause} is <i>not</i>
	 * automatically incorporated in this exception's detail message.
	 *
	 * @param index
	 *            the index of the invalid argument (which is saved for later
	 *            retrieval by the {@link ArgsException#getIndex()} method). A
	 *            value less than {@code 0} is permitted and clamped to
	 *            {@code -1}. This indicates that the index of the argument is
	 *            unknown, or the exception was not caused by a positional
	 *            argument.
	 * @param message
	 *            the detail message (which is saved for later retrieval by the
	 *            {@link Throwable#getMessage()} method).
	 * @param cause
	 *            the cause (which is saved for later retrieval by the
	 *            {@link Throwable#getCause()} method). A {@code null} value is
	 *            permitted, and indicates that the cause is nonexistent or
	 *            unknown.
	 */
	public ArgsException(int index, String message, Throwable cause) {
		super(message, cause);
		this.index = index >= 0 ? index : -1;
		this.opt = null;
	}

	/**
	 * Constructs a new {@code ArgsException} with the specified detail message.
	 *
	 * @param index
	 *            the index of the invalid argument (which is saved for later
	 *            retrieval by the {@link ArgsException#getIndex()} method). A
	 *            value less than {@code 0} is permitted and clamped to
	 *            {@code -1}. This indicates that the index of the argument is
	 *            unknown, or the exception was not caused by a positional
	 *            argument.
	 * @param message
	 *            the detail message (which is saved for later retrieval by the
	 *            {@link Throwable#getMessage()} method).
	 */
	public ArgsException(int index, String message) {
		this(index, message, null);
	}

	/**
	 * Constructs a new {@code ArgsException} with the specified cause.
	 * <p>
	 * Note that the detail message associated with {@code cause} is <i>not</i>
	 * automatically incorporated in this exception's detail message.
	 *
	 * @param index
	 *            the index of the invalid argument (which is saved for later
	 *            retrieval by the {@link ArgsException#getIndex()} method). A
	 *            value less than {@code 0} is permitted and clamped to
	 *            {@code -1}. This indicates that the index of the argument is
	 *            unknown, or the exception was not caused by a positional
	 *            argument.
	 * @param cause
	 *            the cause (which is saved for later retrieval by the
	 *            {@link Throwable#getCause()} method). A {@code null} value is
	 *            permitted, and indicates that the cause is nonexistent or
	 *            unknown.
	 */
	public ArgsException(int index, Throwable cause) {
		this(index, null, cause);
	}

	/**
	 * Constructs a new {@code ArgsException} with no detail message.
	 *
	 * @param index
	 *            the index of the invalid argument (which is saved for later
	 *            retrieval by the {@link ArgsException#getIndex()} method). A
	 *            value less than {@code 0} is permitted and clamped to
	 *            {@code -1}. This indicates that the index of the argument is
	 *            unknown, or the exception was not caused by a positional
	 *            argument.
	 */
	public ArgsException(int index) {
		this(index, null, null);
	}

	/**
	 * Constructs a new {@code ArgsException} with the specified detail message
	 * and cause.
	 * <p>
	 * Note that the detail message associated with {@code cause} is <i>not</i>
	 * automatically incorporated in this exception's detail message.
	 *
	 * @param opt
	 *            the option of the invalid value (which is saved for later
	 *            retrieval by the {@link ArgsException#getOption()} method). A
	 *            {@code null} value is permitted. This indicates that the
	 *            option of the argument is unknown, or the exception was not
	 *            caused by a option.
	 * @param message
	 *            the detail message (which is saved for later retrieval by the
	 *            {@link Throwable#getMessage()} method).
	 * @param cause
	 *            the cause (which is saved for later retrieval by the
	 *            {@link Throwable#getCause()} method). A {@code null} value is
	 *            permitted, and indicates that the cause is nonexistent or
	 *            unknown.
	 */
	public ArgsException(Option opt, String message, Throwable cause) {
		super(message, cause);
		this.index = -1;
		this.opt = opt;
	}

	/**
	 * Constructs a new {@code ArgsException} with the specified detail message.
	 *
	 * @param opt
	 *            the option of the invalid value (which is saved for later
	 *            retrieval by the {@link ArgsException#getOption()} method). A
	 *            {@code null} value is permitted. This indicates that the
	 *            option of the argument is unknown, or the exception was not
	 *            caused by a option.
	 * @param message
	 *            the detail message (which is saved for later retrieval by the
	 *            {@link Throwable#getMessage()} method).
	 */
	public ArgsException(Option opt, String message) {
		this(opt, message, null);
	}

	/**
	 * Constructs a new {@code ArgsException} with the specified cause.
	 * <p>
	 * Note that the detail message associated with {@code cause} is <i>not</i>
	 * automatically incorporated in this exception's detail message.
	 *
	 * @param opt
	 *            the option of the invalid value (which is saved for later
	 *            retrieval by the {@link ArgsException#getOption()} method). A
	 *            {@code null} value is permitted. This indicates that the
	 *            option of the argument is unknown, or the exception was not
	 *            caused by a option.
	 * @param cause
	 *            the cause (which is saved for later retrieval by the
	 *            {@link Throwable#getCause()} method). A {@code null} value is
	 *            permitted, and indicates that the cause is nonexistent or
	 *            unknown.
	 */
	public ArgsException(Option opt, Throwable cause) {
		this(opt, null, cause);
	}

	/**
	 * Constructs a new {@code ArgsException} with no detail message.
	 *
	 * @param opt
	 *            the option of the invalid value (which is saved for later
	 *            retrieval by the {@link ArgsException#getOption()} method). A
	 *            {@code null} value is permitted. This indicates that the
	 *            option of the argument is unknown, or the exception was not
	 *            caused by a option.
	 */
	public ArgsException(Option opt) {
		this(opt, null, null);
	}

	/**
	 * Constructs a new {@code ArgsException} with the specified detail message
	 * and cause.
	 * <p>
	 * Note that the detail message associated with {@code cause} is <i>not</i>
	 * automatically incorporated in this exception's detail message.
	 *
	 * @param message
	 *            the detail message (which is saved for later retrieval by the
	 *            {@link Throwable#getMessage()} method).
	 * @param cause
	 *            the cause (which is saved for later retrieval by the
	 *            {@link Throwable#getCause()} method). A {@code null} value is
	 *            permitted, and indicates that the cause is nonexistent or
	 *            unknown.
	 */
	public ArgsException(String message, Throwable cause) {
		super(message, cause);
		this.index = -1;
		this.opt = null;
	}

	/**
	 * Constructs a new {@code ArgsException} with the specified detail message.
	 *
	 * @param message
	 *            the detail message (which is saved for later retrieval by the
	 *            {@link Throwable#getMessage()} method).
	 */
	public ArgsException(String message) {
		this(message, (Throwable) null);
	}

	/**
	 * Constructs a new {@code ArgsException} with the specified cause.
	 * <p>
	 * Note that the detail message associated with {@code cause} is <i>not</i>
	 * automatically incorporated in this exception's detail message.
	 * 
	 * @param cause
	 *            the cause (which is saved for later retrieval by the
	 *            {@link Throwable#getCause()} method). A {@code null} value is
	 *            permitted, and indicates that the cause is nonexistent or
	 *            unknown.
	 */
	public ArgsException(Throwable cause) {
		this((String) null, cause);
	}

	/**
	 * Constructs a new {@code ArgsException} with no detail message.
	 */
	public ArgsException() {
		this((String) null, (Throwable) null);
	}

	/**
	 * Returns the index of the argument.
	 * 
	 * @return the index, {@code -1} if none was specified.
	 */
	public int getIndex() {
		return this.index;
	}

	/**
	 * Returns the option of the argument.
	 * 
	 * @return the option, {@code null} if none was specified.
	 */
	public Option getOption() {
		return this.opt;
	}

	/**
	 * Returns what value caused the arguments exception.
	 * <p>
	 * What is returned depends on what was specified during the construction of
	 * the exception.
	 * <ul>
	 * <li>If index was specified: {@code "for argument " + index}.</li>
	 * <li>If option was specified:
	 * {@code "for option " + opt.getDisplayName()}.</li>
	 * <li>If neither were specified: {@code null}.</li>
	 * </ul>
	 * 
	 * @return the index or display name of the illegal value.
	 */
	public String getWhat() {
		if (index >= 0) {
			return "for argument " + index;
		} else if (opt != null) {
			return "for option " + opt.getDisplayName();
		}
		return null;
	}

}
