<img src="https://i.imgur.com/VQyoYCT.png" alt=""/>

# Whirvex Args
Whirvex Args is a general purpose arguments parser system. It provides the
ability to easily parse arguments by providing interfaces and standard
implementations for said interfaces. These interfaces can be re-implemented,
or the behavior of the standard implementations overridden. This repository
was created from code originally for [Whirvex CMD](http://gitlab.com/whirvex/whirvex-cmd/whirvex-cmd-java/).

# Features
This arguments parser systems aims to make the parsing of arguments easy,
but also flexible. This is done by providing the following with the API:
- Support for options (named arguments, such as ``-k`` or ``--key``).
- Support for radices (Numerical strings such as ``0b10`` and ``0x10``).
- An argument container class, which can fetch options and convert arguments.
- An arguments parser interface, which parses arguments into a container.

